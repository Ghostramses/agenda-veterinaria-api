//Importar mongoose
const mongoose = require('mongoose');
//Obtener el schema de mongoose
const schema = mongoose.Schema;

//Definir el modelo
const pacientesSchema = new schema({
	nombre: {
		type: String,
		trim: true
	},
	propietario: {
		type: String,
		trim: true
	},
	fecha: {
		type: String,
		trim: true
	},
	telefono: {
		type: String,
		trim: true
	},
	hora: {
		type: String,
		trim: true
	},
	sintomas: {
		type: String,
		trim: true
	}
});

module.exports = mongoose.model('Paciente', pacientesSchema);
