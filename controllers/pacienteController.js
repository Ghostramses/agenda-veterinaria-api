const Paciente = require('../models/Paciente');

// Crea un nuevo cliente
exports.nuevoCliente = async (req, res, next) => {
	//Crear el objeto de paciente con los datos de req.body
	const paciente = new Paciente(req.body);
	try {
		//Guardar el paciente en el modelo
		await paciente.save();
		res.json({ mensaje: 'El cliente se agregó correctamente' });
	} catch (error) {
		console.log(error);
		next();
	}
};

// Obtiene todos los pacientes
exports.obtenerPacientes = async (req, res, next) => {
	try {
		const pacientes = await Paciente.find({});
		res.json(pacientes);
	} catch (error) {
		console.log(error);
		next();
	}
};

// Obtiene un sólo paciente
exports.obtenerPaciente = async (req, res, next) => {
	try {
		const paciente = await Paciente.findById(req.params.id);
		res.json(paciente);
	} catch (error) {
		console.log(error);
		next();
	}
};

// Actualiza un paciente mediante el ID
exports.actualizarPaciente = async (req, res, next) => {
	try {
		const paciente = await Paciente.findOneAndUpdate(
			{ _id: req.params.id },
			req.body,
			{ new: true }
		);
		res.json({ paciente });
	} catch (error) {
		console.log(error);
		next();
	}
};

// Eliminar un paciente mediante el ID
exports.eliminarPaciente = async (req, res, next) => {
	try {
		await Paciente.findOneAndDelete({ _id: req.params.id });
		res.json({ mensaje: 'El paciente ha sido eliminado correctamente' });
	} catch (error) {
		console.log(error);
		next();
	}
};
