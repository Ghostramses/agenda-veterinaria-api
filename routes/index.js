const express = require('express');

const router = express.Router();

//Importar controladores
const pacienteController = require('../controllers/pacienteController');

module.exports = () => {
	//Agregar nuevos pacientes via POST
	router.post('/pacientes', pacienteController.nuevoCliente);

	//Obtener todos los pacientes via GET
	router.get('/pacientes', pacienteController.obtenerPacientes);

	//Obtener un paciente especifico mediante un ID
	router.get('/pacientes/:id', pacienteController.obtenerPaciente);

	// Actualizar un paciente especifico mediante un ID
	router.put('/pacientes/:id', pacienteController.actualizarPaciente);

	// Eliminar un paciente especifico mediante un ID
	router.delete('/pacientes/:id', pacienteController.eliminarPaciente);

	return router;
};
