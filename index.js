const express = require('express');

//importar mongoose
const mongoose = require('mongoose');

//Importar las rutas
const routes = require('./routes');

//Importar body parser
const parser = require('body-parser');

//Importar dotenv

require('dotenv').config();

//Importar cors
const cors = require('cors');

//Crear el servidor
const server = express();

//Habilitar CORS
server.use(cors());

//Conectar a mongodb
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MongoIP, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

//Habilitar el body parser
server.use(parser.json());
server.use(parser.urlencoded({ extended: true }));

//Agregar las rutas
server.use('/', routes());

//Puerto y arrancar el servidor
server.listen(process.env.PORT, process.env.HOST, () => {
	console.clear();
	console.log('Servidor funcionando');
});
